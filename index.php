<!DOCTYPE html>
<html>
	<head>
        <style>
			.container {
            	height: 370px;
            }

            #local, #remote {
                border-style: solid;
                float: left;
                margin-right: 5px;
            }
            
            #local {
                width: 320px;
                height: 240px;
                margin-top: 120px;
            }
            
            #remote {
                width: 480px;
                height: 360px;

            }
            
        </style>
    </head>
    <body>
    	<div class="container">
	        <div id='remote'></div>
	        <div id='local'></div>
        </div>
        <form id='dial'>
            <input type='text' name='number'></input>
            <button type='submit'>Dial</button>
            <button id='hangup'>Hangup</button>

            <!-- enable the current call to be held / resumed -->
            <button type='button' id='hold'>Hold</button>
			<button type='button' id='resume'>Resume</button>
        </form>

        <!-- used to toggle the media state of any new, or existing call -->
        <form id='media'>
		    <input type='checkbox' name='audio' checked>Audio
		    <input type='checkbox' name='video' checked>Video
		</form>

		Resolution:
		<select id="resolution">
		    <option value="videoCaptureResolution176x144">176x144</option>
		    <option value="videoCaptureResolution352x288">352x288</option>
		    <option value="videoCaptureResolution320x240">320x240</option>
		    <option value="videoCaptureResolution640x480">640x480</option>
		    <option value="videoCaptureResolution1280x720">1280x720</option>
		    <option value="videoCaptureResolution1280x720">1920x1080</option>
		</select>
		
		FPS:
		<select id="fps">
		    <option value="10">10</option>
		    <option value="20">20</option>
		    <option value="30">30</option>
		</select>
    </body>

	<!-- Import the FCSDK JavaScript libraries -->
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/adapter.js'></script>
	<script type='text/javascript' src='http://fusion.vbox:8080/gateway/fusion-client-sdk.js'></script>

	<!-- Although not required by the FCSDK, jQuery will ease interactions with the DOM -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

	<script>
		<?php 
			// import our init script
			include_once('init.php');

			// determine the parameters to use for the session
			$username = array_key_exists('username', $_GET) 
				? $_GET['username'] 
				: 'nathan';
			$password = array_key_exists('password', $_GET)
				? $_GET['password'] 
				: 'abc';

			// provision the session
			$token = provision($username, $password);
		?>
		
		var token = <?= $token ?>;

		// initialise the connection to the Gateway
		UC.start(token.sessionid);

		// when available, direct the local media stream to the right DOM element
	    var local = $('#local')[0];
	    var remote = $('#remote')[0];
	    UC.phone.setPreviewElement(local);

	    // wire up the call handler
	    var $mediaForm = $('#media');
	    $('#dial').submit(function (event) {
		    event.preventDefault();
		    var number = $(this).find('[name=number]').val();

		    // create the call
		    var call = UC.phone.createCall(number);

		    // add listeners
		    addListenersTo(call);
		    
		    // set the remote video element
		    call.setVideoElement(remote);

		    // determine whether to initiate the call with video / audio
		    var audio = $mediaForm.find('[name=audio]').prop('checked');
    		var video = $mediaForm.find('[name=video]').prop('checked');
		    
		    // now dial the call
		    call.dial(audio, video);
		});

	    // wire up the hangup handler
		$('#hangup').click(function(event) {
			// prevent the button from causing the form to submit
			event.preventDefault();

		    // if we're on a call - end it!
		    var calls = UC.phone.getCalls();
		    var call = calls[0];
		    if (call) {
		        call.end();

		        // ensure that the code to tidy up the media preview
		        // is invoked. Since this client is the one to be ending
		        // the call, the onEnded callback will not be made by the
		        // SDK - therefore, we will want to trigger it ourselves.
		        call.onEnded();
		    }
		});

		// setup an incoming call handler
		UC.phone.onIncomingCall = function(call) {
			// prompt the user to accept or reject the incoming call
		    if (window.confirm('Answer call from: ' + call.getRemoteAddress() + '?')) {
		    	// set the target for remote video & answer the call
		    	call.setVideoElement(remote);

		    	// add listeners
		    	addListenersTo(call);

		    	// determine whether to answer the call with audio / video
			    var audio = $mediaForm.find('[name=audio]').prop('checked');
	    		var video = $mediaForm.find('[name=video]').prop('checked');

		    	// answer the call
		        call.answer(audio, video);
		    }
		    else {
		    	// reject the call
		        call.end();
		    }
		};

		// toggle media capabilities
		$('#media').change(function () {
		    // work out the current settings
		    var $this = $(this);
		    var audio = $this.find('[name=audio]').prop('checked');
		    var video = $this.find('[name=video]').prop('checked');

		    // only apply if a call is active
		    var call = UC.phone.getCalls()[0];
		    if (call) {
		        call.setLocalMediaEnabled(video, audio);
		    }
		});

		// since this application's implementation only supports a single call
		// at a time, we can assume that this request should apply to the first
		// call in the list - assuming there is one;

		// the hold handler
		$('#hold').click(function() {
		    var call = UC.phone.getCalls()[0];
		    if (call) {
		        call.hold();
		    }
		});

		// the resume handler
		$('#resume').click(function() {
		    var call = UC.phone.getCalls()[0];
		    if (call) {
		        call.resume();
		    }
		});

		// assigns listeners to the call
		function addListenersTo(call) {
			// placeholder for adding call specific callback functions
			call.onEnded = function () {
			    $(remote).find('video').attr('src', '');
			};
		}

		// adjust the resolution
		$('#resolution').change(function (event) {
		   var index = $(this).val();
		   var res = UC.phone.videoresolutions[index];
		   UC.phone.setPreferredVideoCaptureResolution(res);
		});

		// adjust the framerate
		$('#fps').change(function (event) {
		   var fps = Number( $(this).val() );
		   UC.phone.setPreferredVideoFrameRate(fps);
		});

		// ensure that the current preferred resolutions match the values set
		$('#fps, #resultion').trigger('change');


	</script>
</html>